/**
 * Created by Anshul on 26-02-2017.
 */
import { Template } from 'meteor/templating';

import './body.html';

Template.body.helpers({
    trades: [
        { text : 'This is Trade #1!' },
        { text : 'This is Trade #2!' },
        { text : 'This is Trade #3!' },
    ],
});